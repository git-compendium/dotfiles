# Dotfiles config

## From scratch

```bash
git init --bare $HOME/.cfg
alias config='/usr/bin/git --git-dir=$HOME/.cfg/ --work-tree=$HOME'
config config --local status.showUntrackedFiles no
echo "alias config='/usr/bin/git --git-dir=$HOME/.cfg/ --work-tree=$HOME'" >> $HOME/.bashrc
```

## Install on a new system

```bash
alias config='/usr/bin/git --git-dir=$HOME/.cfg/ --work-tree=$HOME'
echo ".cfg" >> $HOME/.gitignore
git clone --bare https://gitlab.com/gitbuch/dotfiles.git $HOME/.cfg
config checkout
if [ "$?" -gt 0 ]
then
  mkdir $HOME/.dotfiles.bup
  config checkout 2>&1 | grep "^[[:space:]]" \
    | xargs -I{} mv -v {} $HOME/.dotfiles.bup/{}
fi
config config --local status.showUntrackedFiles no
## vim setup with submodules (credits https://github.com/jessfraz)
config submodule update --init
```


## Zsh and Oh-my-Zsh

```bash
sudo apt install zsh
## oh-my-zsh
sh -c "$(wget https://raw.githubusercontent.com/robbyrussell/oh-my-zsh/master/tools/install.sh -O -)"
mv .zshrc.pre-oh-my-zsh .zshrc
```

## Yubikey

```bash
sudo apt-get install gnupg2 pcscd scdaemon
echo "personal-digest-preferences SHA256" >> ~/.gnupg/gpg.conf                             
echo "cert-digest-algo SHA256" >> ~/.gnupg/gpg.conf
echo "default-preference-list SHA512 SHA384 SHA256 SHA224 AES256 AES192 AES CAST5 ZLIB BZIP2 ZIP Uncompressed" >> ~/.gnupg/gpg.conf
echo "keyserver hkp://keys.gnupg.net" >> ~/.gnupg/gpg.conf
echo "enable-ssh-support" >> ~/.gnupg/gpg-agent.conf
```

## System config

```bash
sudo gpasswd -a $USER input  # add my user to input group
sudo apt-get install libinput-tools xdotool feh acpi sysstat lm-sensors arandr virtualbox \
  ruby i3 i3status pasystray scrot gimp htop blueman pavucontrol vim nextcloud-desktop \
  fonts-font-awesome compton keepassxc chromium-browser flameshot git-lfs neovim xsel
sudo gem install fusuma
```

## Sysadmin tools

```bash
sudo apt install wireguard ansible autofs whois ipcalc docker-compose pwgen
```

## Android development

### Udev Rules

```bash
# Fairphone3
echo 'SUBSYSTEM=="usb", ATTR{idVendor}=="18d1", ATTR{idProduct}=="4ee7", MODE="0666", GROUP="plugdev"' \
  > /etc/udev/rules.d/51-android.rules
udevadm control --reload-rules
```

### Accept android licenses
```bash
Android/Sdk/tools/bin/sdkmanager --licenses
```
